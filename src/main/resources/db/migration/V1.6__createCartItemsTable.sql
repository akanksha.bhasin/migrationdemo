CREATE TABLE CART_ITEMS(
    id varchar(50),
    cart_id varchar(50),
    product_id varchar(50),
    quantity int
);